//
//  ViewController.swift
//  PhotoGallery
//
//  Created by Ashish on 06/05/16.
//  Copyright © 2016 Ashish. All rights reserved.
//

import UIKit

class ViewController: UIViewController  {
    
    @IBOutlet weak var imageView: UIImageView!
    override func viewDidLoad() {
        
        super.viewDidLoad()
    }
    

    @IBAction func pickButton(sender: AnyObject) {
        
        
        let photo = PhotoGalleryLib.sharedInstance()
        
        photo.photoCompletionHandler = { (image: UIImage?,videoUrl: NSURL!) -> Void in
            
            if  videoUrl == nil { //--Image
                
                self.imageView.image = image
                print("Selected Image is :\(image)")
                print("Selected  Url is :\(videoUrl)")
                UIImageWriteToSavedPhotosAlbum(image!, nil, nil, nil);
            }
            else{ //--Video
                
                print("Selected Video Url is :\(videoUrl)")
                print("Selected Video Thumbnail is :\(image)")
                self.imageView.image = image
                UIImageWriteToSavedPhotosAlbum(image!, nil, nil, nil);
                
                photo.playVideo(self, videoUrl: videoUrl, callBack: { (finish) in
                    
                    print(finish!)
                })
            }
        }
        
        photo.cancelCompletionHandler = {
            print("Cancel Pressed")
        }
        
        photo.errorCompletionHandler = { (error: PhotoGalleryLib.ErrorPhotoPicker!) -> Void in
            print("Error: \(error.rawValue)")
        }
        
        photo.present(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

