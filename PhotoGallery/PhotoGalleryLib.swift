//
//  PhotoGalleryLib.swift
//  PhotoGallery
//
//  Created by Ashish on 06/05/16.
//  Copyright © 2016 Ashish. All rights reserved.
//

import UIKit

import Photos
import MobileCoreServices
import AssetsLibrary

import AVKit
import AVFoundation

public enum SelectionType{
    case PhotoOnly
    case VideoOnly
    case Both
}

struct videoInfo {
    
    let videoUrl: NSURL
    let videoThumbnail: UIImage!
}


var photoLIbContr = PhotoGalleryLib()

 class PhotoGalleryLib: NSObject{

    private static var sharedInstanceVar = PhotoGalleryLib()
    
    internal class func sharedInstance()->PhotoGalleryLib {
        return sharedInstanceVar
    }
    
    /**
     List of callbacks variables
     */
    var photoCompletionHandler: ((image: UIImage?,videoUrl: NSURL!) -> Void)?
    var presentedCompletionHandler: (() -> Void)?
    var cancelCompletionHandler: (() -> Void)?
    var errorCompletionHandler: ((error: ErrorPhotoPicker!) -> Void)?
    
     enum ErrorPhotoPicker: String {
        
        case CameraNotAvailable = "Camera not available"
        case Other = "Other"
    }
    

    func present(controller1: UIViewController!) -> Void {

        self.controller = controller1
        let alertVwController = UIAlertController(title:"Select your item", message:nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        //--A) Camera Section ***************
        let cameraButtonAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.Default) { (UIAlertAction) in
            
            if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)){
                
                self.presentImagePicker(.Camera, topController: self.controller)
            }
            else{
                
                self.errorCompletionHandler?(error: .CameraNotAvailable)
            }
        }
        
        //--B) Gallery Section ***************
        let openGalleryButtonAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.Default) { (UIAlertAction) in
           
            self.presentImagePicker(.PhotoLibrary, topController: self.controller)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel)
        {
            UIAlertAction in
            self.cancelCompletionHandler?()
        }
        
        alertVwController.addAction(cameraButtonAction)
        alertVwController.addAction(openGalleryButtonAction)
        alertVwController.addAction(cancelAction)
        
        self.controller!.presentViewController(alertVwController, animated: true) { () -> Void in
            self.presentedCompletionHandler?()
        }
    }
    
    
    var controller: UIViewController?
    var imagePicker: UIImagePickerController!

    func presentImagePicker(sourceType: UIImagePickerControllerSourceType, topController: UIViewController!) {
        
            self.imagePicker = UIImagePickerController()
            self.imagePicker.delegate = PhotoGalleryLib.sharedInstance()
            self.imagePicker.editing = true
            self.imagePicker.sourceType = sourceType
            self.imagePicker.mediaTypes = [kUTTypeMovie as String, kUTTypeImage as String]
            topController.presentViewController(self.imagePicker, animated: true, completion: nil)
    }
}


extension PhotoGalleryLib: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
      func imagePickerControllerDidCancel(picker: UIImagePickerController) {
       
        print("Cancel Method")
        picker.dismissViewControllerAnimated(true) { () -> Void in
            
            print("Inner Cancel Method")
            self.cancelCompletionHandler?()
        }
    }
    
    
    func getVideoThumbnail(fileUrl:NSURL, callBack:(thumbImage : UIImage?) -> ()) {
        
        var videoThumbnail: UIImage?
        let asset = AVAsset(URL: fileUrl)
        let assetImgGenerate = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        let time = CMTimeMake(asset.duration.value / 3, asset.duration.timescale)
        if let cgImage = try? assetImgGenerate.copyCGImageAtTime(time, actualTime: nil) {
            
            videoThumbnail = UIImage(CGImage: cgImage)
            callBack(thumbImage: videoThumbnail)
        }
    }
    

      func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
       
        let mediaType = info[UIImagePickerControllerMediaType] as! NSString
        if mediaType.isEqualToString(kUTTypeImage as String) {
            
            // Media is an image
            if let image = info[UIImagePickerControllerOriginalImage] {
                
                PhotoGalleryLib.sharedInstance().photoCompletionHandler?(image: image as? UIImage, videoUrl: nil)
                
            } else {
                
                PhotoGalleryLib.sharedInstance().errorCompletionHandler?(error: .Other)
            }
            picker.dismissViewControllerAnimated(true, completion: nil)
            
        } else if mediaType.isEqualToString(kUTTypeMovie as String) {
            
            // Media is a video
            if let selectedVideo = info[UIImagePickerControllerMediaURL] {
               
                self.getVideoThumbnail(selectedVideo as! NSURL, callBack: { (thumbImage) in
                    
                    //print("Video Thumbnail is : \(thumbImage)")
                    picker.dismissViewControllerAnimated(true, completion: nil)
                    PhotoGalleryLib.sharedInstance().photoCompletionHandler?(image: thumbImage , videoUrl: selectedVideo as! NSURL)
                })
                
            } else {
                
                PhotoGalleryLib.sharedInstance().errorCompletionHandler?(error: .Other)
                picker.dismissViewControllerAnimated(true, completion: nil)
            }
            
        }
    }
    
}


extension PhotoGalleryLib{
    
    func playVideo(controller:UIViewController,videoUrl: NSURL?, callBack:(finish : NSString?) -> ()) {

        var playerItem:AVPlayerItem!

        playerItem = AVPlayerItem(URL: videoUrl!)
        
        let player = AVPlayer(playerItem: playerItem)
        let playerController = AVPlayerViewController()
        
        playerController.player = player
        controller.addChildViewController(playerController)
        controller.view.addSubview(playerController.view)
        playerController.view.frame = controller.view.frame
        
        player.play()
        
        let center = NSNotificationCenter.defaultCenter()
        center.addObserverForName("AVPlayerItemDidPlayToEndTimeNotification", object: playerItem, queue: nil) { (notification) -> Void in
            
            playerController.view .removeFromSuperview()
            callBack(finish: "Video playing finish")
        }
        
    }

}














